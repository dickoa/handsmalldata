
# handsmalldata

<!-- README.md is generated from README.Rmd. Please edit that file -->

[![GitLab CI Build
Status](https://gitlab.com/dickoa/handsmalldata/badges/master/build.svg)](https://gitlab.com/dickoa/handsmalldata/pipelines)
[![Codecov Code
Coverage](https://codecov.io/gl/dickoa/handsmalldata/branch/master/graph/badge.svg)](https://codecov.io/gl/dickoa/handsmalldata)
[![License:
MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)

R package providing all 510 data sets from Hand, et. al., 1994, A
Handbook of Small Data Sets

## Installation

This package is not on yet on CRAN and to install it, you will need the
[`remotes`](https://github.com/r-lib/remotes) package.

``` r
## install.packages("remotes")
remotes::install_gitlab("dickoa/handsmalldata")
```

## Meta

  - Please [report any issues or
    bugs](https://gitlab.com/dickoa/handsmalldata/issues).
  - License: MIT
  - Please note that this project is released with a [Contributor Code
    of Conduct](CONDUCT.md). By participating in this project you agree
    to abide by its terms.
